//
//  Utility.swift
//  RegistrationApp
//
//  Created by Chhaya on 11/12/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import Foundation
import UIKit

class Util {
    
    // Create static label for using in class
    static let label = UILabel()
    
    // alertMessage() : output warning message by using label to place anywhere in any form
    static func alertMessage(message: String, view: UIView?, x: Int, y: Int) {
        label.frame = CGRect(x: x, y: y, width: 335, height: 20)
        label.text = message
        label.textColor = #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)
        label.font = label.font.withSize(12)
        view?.addSubview(label)
    }
    
    // clearAlertMessage() : delete all messages that have outputted on any form
    static func clearAlertMessage() {
        label.text = ""
    }
    
}
