//
//  LoginViewController.swift
//  RegistrationApp
//
//  Created by CHHAYA on 11/11/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // IBOutlet properties
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    // Customized properties
    var subDelegate : SubUITextFieldDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Action of login button
    @IBAction func loginButtonTap(_ sender: UIButton) {
        
        // This condition works only when login is successfully with good data.
        // When data is good, it will work.
        if(loginWithRightData() == true) {
            subDelegate.getText(userNameTextField.text)
            dismiss(animated: true, completion: nil)
            userNameTextField.text = ""
            passwordTextField.text = ""
        }
        
    }
    
    // Action of sign up button
    // It is used to create a new user
    @IBAction func singupButtonTap(_ sender: UIButton) {
        let singupViewController = storyboard?.instantiateViewController(withIdentifier: "signUpViewController") as! SignupViewController
        show(singupViewController, sender: nil)
        userNameTextField.text = ""
        passwordTextField.text = ""
        Util.clearAlertMessage()
    }
    
    
    // loginUserAccount() is used to login with the user that have in Class Account
    func loginUserAccount(_ username: String?, _ password: String?) -> Bool {
        
        let size = Accounts.accounts.count
        var index = 0
        
        while index < size {
            if let username = username, let password = password, username == Accounts.accounts[index].username, password == Accounts.accounts[index].password {
                print(username, password)
                return true
            }
            index = index + 1
        }
        return false
        
    }
    
    // Validation from user's input to make sure that it can be logged into system or not
    // (1) If not, it will return warning message by using Util.alertMessage()
    // When using the function again, it will clear warning message and follow the process (1) again
    func loginWithRightData() -> Bool {
        
        Util.clearAlertMessage()
        
        guard let username = userNameTextField.text, !username.isEmpty else {
            Util.alertMessage(message: "Please enter username", view: view, x: 20, y: 376)
            return false
        }
        
        guard let password = passwordTextField.text, !password.isEmpty else {
            Util.alertMessage(message: "Please enter password", view: view, x: 20, y: 376)
            return false
        }
        
        guard loginUserAccount(username, password) == true else {
            Util.alertMessage(message: "The account that you have entered is incorrect.", view: view, x: 20, y: 376)
            return false
        }
        
        return true
    }
    
}

