//
//  SignupViewController.swift
//  RegistrationApp
//
//  Created by CHHAYA on 11/11/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {

    // IBOutlet properties
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Action of sign up button
    @IBAction func singupButtonTap(_ sender: UIButton) {
        
        // This statement will work only the right data is submitted
        if(submitRightData() == true) {
            dismiss(animated: true, completion: nil)
        }

    }
    
    // Action of cancel button to go back to user login form
    @IBAction func cancelButtonTap(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // Append user structure to Account class
    func createAccount(user: User) {
        Accounts.accounts.append(user)
    }

    // Validation from user's input to make sure that it can be signed up into system or not
    // (1) If not, it will return warning message by using Util.alertMessage()
    // When using the function again, it will clear warning message and follow the process (1) again
    func submitRightData() -> Bool{
        
        Util.clearAlertMessage()
        
        
        guard let username = usernameTextField.text, !username.isEmpty else {
            Util.alertMessage(message: "Please enter username.", view: view, x: 20, y: 210)
            return false
        }
        
        guard  username.count > 5 else {
            Util.alertMessage(message: "Username must be at least 6 characters.", view: view, x: 20, y: 210)
            return false
        }
        
        guard let password = passwordTextField.text, !password.isEmpty else {
            Util.alertMessage(message: "Please enter password.", view: view, x: 20, y: 295)
            return false
        }
        
        guard  password.count > 5 else {
            Util.alertMessage(message: "Password must be at least 6 characters.", view: view, x: 20, y: 210)
            return false
        }

        guard let confirm = confirmPasswordTextField.text, !confirm.isEmpty else {
            Util.alertMessage(message: "Please enter confirm password.", view: view, x: 20, y: 380)
            return false
        }
        
        guard password == confirm else {
            Util.alertMessage(message: "Password is not matched.", view: view, x: 20, y: 380)
            return false
        }
        
        let user = User.init(username: username, password: password)
        createAccount(user: user)
        return true
        
    }
    
}
