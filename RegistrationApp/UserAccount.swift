//
//  Users.swift
//  RegistrationApp
//
//  Created by CHHAYA on 11/11/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import Foundation

// Storing data of user in Account
struct User {
    var username: String
    var password: String
}

// Storing each user that have created
class Accounts {
    static var accounts: [User] = []
}

