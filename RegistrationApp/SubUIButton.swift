//
//  SubUIButton.swift
//  RegistrationApp
//
//  Created by CHHAYA on 11/11/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import Foundation
import UIKit

// Extend from UIButton to add some behaviour of UIButton
extension UIButton {
    
    open override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
    }

}
