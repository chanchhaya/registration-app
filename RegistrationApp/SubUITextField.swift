//
//  SubUITextField.swift
//  RegistrationApp
//
//  Created by CHHAYA on 11/11/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import Foundation
import UIKit

// This protocol have a properties that is used to delegate text from one view to one view.
// Example : Delegate String "username" when login successfully.
protocol SubUITextFieldDelegate {
    
    func getText(_ text: String?)
    
}

// Extend from UITextField to add some behaviour of UITextField.
extension UITextField {
    
    open override func draw(_ rect: CGRect) {
        self.frame.size.height = 50
    }
    
}
