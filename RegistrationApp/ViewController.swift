//
//  ViewController.swift
//  RegistrationApp
//
//  Created by CHHAYA on 11/10/18.
//  Copyright © 2018 CHHAYA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    // IBOutlet properties
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initial empty string to username label
        usernameLabel.text = ""
        
    }
    
    // Action of login button in main form
    @IBAction func loginButtonTap(_ sender: UIButton) {
        
        // Case login button
        if (loginButton.currentTitle == "Login") {
            let loginViewController = storyboard?.instantiateViewController(withIdentifier: "logInViewController") as! LoginViewController
            loginViewController.subDelegate = self
            show(loginViewController, sender: nil)
        } else { // Case logout button
            loginButton.setTitle("Login", for: .normal)
            loginButton.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.38)
            usernameLabel.text = ""
            profileImage.isHidden = true
        }
    }

}

// Extension from SubUITextFieldDelegate
extension ViewController: SubUITextFieldDelegate {
    // Function to get data from login form successfully by using delegate
    func getText(_ text: String?) {
        loginButton.setTitle("Logout", for: .normal)
        loginButton.backgroundColor = #colorLiteral(red: 0.6034238338, green: 0.1934605241, blue: 0.08918578178, alpha: 1)
        usernameLabel.text = "Welcome " + text!
        profileImage.isHidden = false
    }
    
}

